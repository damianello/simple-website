<?php
/*
 * Template Name: technologia-page
 */
get_header(); ?>
<div class="header_image_pages" style="background-image:url('<?php echo content_url(); ?>/uploads/technologia_header.png')">

</div>
<div class="all">
    <div id="main">
        <div id="content">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <h1><?php the_title(); ?></h1>
                <div class="titleBackground">

                </div>
                <p><?php the_content(); ?></p>
            <?php endwhile; endif; ?>
        </div>
    </div>
<?php get_footer(); ?>
