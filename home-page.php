<?php
/*
 * Template Name: home-page
 */
get_header();
 ?>
 <div class="header_image" style="background-image:url('<?php echo content_url(); ?>/uploads/headerbg.jpg')">

 </div>
<div class="home_links">
    <div class="home_links_item">
        <img src="<?php echo content_url(); ?>/uploads/tech_home.jpg">
        <a href="/technologia" class="inside_home_links">
            Technologia
        </a>
    </div>
    <div class="home_links_item">
        <img src="<?php echo content_url(); ?>/uploads/oferta_home.jpg">
        <a href="/oferta" class="inside_home_links">
            Oferta
        </a>
    </div>
    <div class="home_links_item">
        <img src="<?php echo content_url(); ?>/uploads/kontakt_home.jpg">
        <a href="/kontakt" class="inside_home_links">
            Kontakt
        </a>
    </div>
</div>
<div class="home_content">
    <div class="home_content_item">
        <div class="home_content_item_line">

        </div>
        <h3>KAPEO <span style="color:red">Kolor</span><br/>śrutowanie i malowanie proszkowe</h3>
        <div class="home_content_item_text">
            <?php echo do_shortcode('[jcf-value field="_field_textarea__1498118684"]'); ?>
        </div>
    </div>
    <div class="home_content_item">
        <div class="home_content_item_line">

        </div>
        <h3>Stosujemy produkty wiodących producentów o najwyższej jakości.</h3>
        <img style="padding:0 25px;" src="<?php echo content_url(); ?>/uploads/logo1.png">
        <img style="padding:0 25px;" src="<?php echo content_url(); ?>/uploads/logo2.png">
        <img style="padding:0 25px;" src="<?php echo content_url(); ?>/uploads/logo3.png">
        <div class="home_content_item_text">
            <?php echo do_shortcode('[jcf-value field="_field_textarea__1498118640"]'); ?>
        </div>
    </div>
    <div class="home_content_item">
        <div class="home_content_item_line">

        </div>
        <h3>Zapraszamy do współpracy</h3>
        <div class="home_content_item_text">
            <?php echo do_shortcode('[jcf-value field="_field_textarea__1498118693"]'); ?>
        </div>
    </div>
</div>
 <?php

get_footer();
?>
