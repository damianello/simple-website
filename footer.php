        <?php wp_footer(); ?>
            </div>
        <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>">

        <div class="push"></div>
        <footer class="footer">
            <div class="footer_image" style="background-image:url('<?php echo content_url(); ?>/uploads/footer.png')">

            </div>
            <div class="footer_logos">
                <div class="footer_content">
                    <div class="left_footer">
                        <span>Partnerzy</span>
                    </div>
                    <div class="footer_logos_center">
                        <div class="logo_in_footer">
                            <img src="<?php echo content_url(); ?>/uploads/logo_f_01.png">
                        </div>
                        <div class="logo_in_footer">
                            <img src="<?php echo content_url(); ?>/uploads/logo_f_02.png">
                        </div>
                        <div class="logo_in_footer">
                            <img src="<?php echo content_url(); ?>/uploads/logo_f_03.png">
                        </div>
                    </div>
                    <div class="footer_right">
                        <span>&copy; 2017 all rights reserved</span>
                    </div>
                </div>
            </div>
        </footer>


    <script type="text/javascript">
      var menu_button = document.getElementById('menu_trigger');
      var menu = document.getElementById('head_nav');
      menu_button.addEventListener('click', function() {
        if (menu.classList.contains('show')) {
          menu.style.display = 'none';
          menu.classList.add('hide');
          menu.classList.remove('show');
          return;
        }
        menu.style.display = 'block';
        menu.classList.add('show');
        menu.classList.remove('hide');
      });
      var fbLogo = document.querySelectorAll('.fbImage');
      var fbEl = document.querySelectorAll('.facebook');
      var fbPage = document.querySelectorAll('.fb-page');
      console.log(fbLogo[0]);
      console.log(fbEl[0]);
      fbLogo[0].addEventListener('mouseover', function(){
          fbEl[0].style.right = '300px';
      });
      fbPage[0].addEventListener('mouseleave', function(){
          fbEl[0].style.right = '0';
      });
    </script>
    </body>
</html>
