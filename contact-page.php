<?php
/*
 * Template Name: kontakt-page
 */
get_header();
 ?>
 <div class="header_image_pages" style="background-image:url('<?php echo content_url(); ?>/uploads/headerbg.jpg')">

 </div>
 <div id="content" class="kontakt_content">
     <h1>Kontakt</h1>
     <div class="titleBackground">

     </div>
     <div class="push_kontakt">

     </div>
     <div class="kontakt_image">
         <img src="<?php echo content_url(); ?>/uploads/firma.jpg">
     </div>
     <div class="first_kontakt">
         <p>
             <span>KAPEO Kolor Sp. z o.o.</span>
             <span>Kś. A. Peplińskiego 10</span>
             <span>83-321 Mściszewice</span>

             <span>+48 (58) 685 41 81</span>
             <span><a href="mailto:biuro@kapeokolor.pl?subject=kontakt">biuro@kapeokolor.pl</a></span>
         </p>
     </div>
     <div class="kontakt_line">

     </div>
     <div class="second_kontakt">
         <p>
             <span>Maciej Sieciński</span>
             <span>Dyrektor marketingu</span>
             <span>+48 697 575 720</span>
             <span><a href="mailto:maciej.siecinski@kapeokolor.pl?subject=kontakt">maciej.siecinski@kapeokolor.pl</a></span>
         </p>
     </div>
     <div class="map">
         <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
             <?php the_content(); ?>
         <?php endwhile; endif; ?>
     </div>
</div>
 <?php

get_footer();
?>
