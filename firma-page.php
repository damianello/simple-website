<?php
/*
 * Template Name: firma-page
 */
get_header(); ?>
<div class="header_image_pages" style="background-image:url('<?php echo content_url(); ?>/uploads/headerbg.jpg')">

</div>
<div class="all">
    <div id="main">
        <div id="content" class="firma">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <h1><?php the_title(); ?></h1>
                <div class="titleBackground">

                </div>
                <div class="firma_content"><?php the_content(); ?></div>
                <div class="firma_line">
                    <img src="<?php echo content_url(); ?>/uploads/firma.jpg">
                </div>
            <?php endwhile; endif; ?>
        </div>
    </div>
<?php get_footer(); ?>
