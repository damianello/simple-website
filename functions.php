<?php
add_theme_support('post-thumbnails');
add_theme_support('menus');

function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Add Shortcode
function techno_shortcode( $atts , $content = null ) {

    ob_start();

	// Attributes
	$atts = shortcode_atts(
		array(
			'tytul' => '',
            'zdjecie' => ''
		),
        $atts
	);

    printf('<div class="techDiv">' . $atts['zdjecie'] . '<h3>' . $atts['tytul'] . '</h3><p>' . $content . '</p></div>');

    return ob_get_clean();

}
add_shortcode( 'techno', 'techno_shortcode' );

function ofert_shortcode( $atts , $content = null ) {

    ob_start();

	// Attributes
	$atts = shortcode_atts(
		array(
            'zdjecie' => ''
		),
        $atts
	);

    printf('<div class="techDiv uslDiv">' . $atts['zdjecie'] . '<p>' . $content . '</p>' . '</div>');

    return ob_get_clean();

}
add_shortcode( 'ofert', 'ofert_shortcode' );

?>
