<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?php echo wp_title() ?></title>
        <meta name = "viewport" content = "user-scalable=no, initial-scale=1.0, maximum-scale=1.0, width=device-width">
        <?php wp_head(); ?>
    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=1734683720131375";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        </script>
        <div class="wrapper">
        <header>
            <a id="logo" href="/"><img src="<?php echo content_url(); ?>/uploads/kapeokolor_logo.png" alt="Kapeo Kolor"></a>
            <div id="menu_trigger">
              <img src="<?php echo content_url(); ?>/uploads/menu.png" alt="Kapeo Kolor">
            </div>
            <nav id="head_nav">
                <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
            </nav>
        </header>
        <div class="facebook">
            <img class="fbImage" src="<?php echo content_url(); ?>/uploads/facebook.png">
            <div class="fb-page" data-href="https://www.facebook.com/kapeokolor.sp.zoo/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/kapeokolor.sp.zoo/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/kapeokolor.sp.zoo/">KAPEO Kolor Sp zoo</a></blockquote></div>
        </div>
